# Build Custom CLI Operation
This project is a LabVIEW Class that will run as Custom Operation using the NI LabVIEW CLI application.\
It will call the Build Specification API.

# Author
Felipe Pinheiro Silva

## Installation

To install you may use the NI Package Manager available in the [releases page](../../releases).

## Dependencies
### Packages
- LabVIEW Command Line Interface Package
- LabVIEW 2020

## Testing
For testing this project uses:
- Static Code - VI Analyzer
- Unit Test - Caraya Framework

## Usage
Operation Name - **BuildCustom**.\
Builds an application, a library, or a bitfile using the settings in the specified build specification(s) and returns the path of the output files.
The following table lists the available arguments of this operation. Required arguments are in **bold**.

| Argument | Description | Default |
| :-------- | :----------- | :------- |
| -ProjectPath | 	Path to the LabVIEW project (.lvproj) file that contains the build specification | If not specified in Gitlab uses the environment variable CI_PROJECT_DIR and gets the first project file (.lvproj) found. |
| -Version | 	String version in the semantic version (X.Y.Z) of the build specification, the build field is optional. |  Version specified in the buildspec |
| −TargetName |	Target that contains the build specification. |	My Computer |
| −BuildSpecName	| Name of the build specification that appears under **Build Specifications** in the Project Explorer window. | Empty string—The CLI for LabVIEW builds all build specifications under the specified target. |

`LabVIEWCLI -OperationName BuildCustom -ProjectPath <path to project> -TargetName <name of target> -BuildSpecName <name of build specification>`

## Contributing
Merge requests are welcome. For major changes, please open an issue first to discuss what you would like to change.\
Please make sure to update tests as appropriate.

## License
[BSD3](LICENSE)
